//
//  HSBCGlobalInstance.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Commondefine.h"
#import "SellerModel.h"


@interface HSBCGlobalInstance : NSObject

DECLARE_SINGLETON_FOR_CLASS(HSBCGlobalInstance)

@property (nonatomic , strong) NSString * netWorkToken; //   网络请求token
@property (nonatomic , assign)  BOOL needMock;

@property (nonatomic , strong) SellerModel * curSellerModel; //  当前登录用户

@property (nonatomic , assign) BOOL isShowLibrary;//是否启用了相机
@end
