//
//  AccountModule.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModule.h"

@interface AccountModule : BaseModule

/*
 * hr 注册
 *@param
 *      companyId  企业id
 *      phone  hr 电话
 *      password 登录密码
 *      verificationCode   验证码
 */
+ (void)registerWithPhone:(NSString *)phone
                     password:(NSString *)password
                   verifyCode:(NSString *)verificationCode
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock;

/*
 * 找回密码
 * @param
 *   pwd_  新密码
 *   phone   手机号
 *   verificationCode   验证码
 */

+ (void)resetPasswordWithNewPwd:(NSString *)pwd_
                          phone:(NSString *)phone
                     verifyCode:(NSString *)verificationCode
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/**
 *找回密码下一步
 *@param
 *     pwd_ 新密码
 *     newPwd  确认新密码
 *     token   token
 */
+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd
                           token:(NSString *)token
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;


/*
 * 用户登录
 *  @param
 *         phone  手机号
 *         pwd_  登录密码
 *         code  验证码
 */

+ (void)loginWithPhone:(NSString *)phone
                   password:(NSString *)pwd_
            verifyCode:(NSString *)code
               success:(RequestSuccessBlock)succBlock
               failure:(RequestFailureBlock)failBlock;


/*
 *  获取短信验证码
 */
+ (void)getSMSVerifyCodeWithPhone:(NSString *)phone
                             type:(NSString *)type
                          success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBloc;
@end
