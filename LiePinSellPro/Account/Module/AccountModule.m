//
//  AccountModule.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AccountModule.h"
#import "SellerModel.h"

@implementation AccountModule

+ (void)registerWithPhone:(NSString *)phone
                 password:(NSString *)password
               verifyCode:(NSString *)verificationCode
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"password" :password,
                            @"cellphone":phone,
                            @"verificationCode":verificationCode
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"auth/marketerRegister" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * user = response_[@"user"];
        if (user) {
            SellerModel * model = [SellerModel modelWithDictionary:user];
            model.networkToken = token;
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curSellerModel = model;
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)resetPasswordWithNewPwd:(NSString *)pwd_
                          phone:(NSString *)phone
                     verifyCode:(NSString *)verificationCode
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"platformType":@"PLATFORM_MARKECTER",
                            @"cellphone":phone,
                            @"verificationCode":verificationCode,
                            };
    
    [NetworkHandle postRequestForApi:@"auth/setPassword" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary *dict = (NSDictionary *)response;
        NSString *token = dict[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
            return @[@YES,token];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd token:(NSString *)token success:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"password":newPwd,
                            @"platformType":@"PLATFORM_MARKECTER",
                            @"token":token,
                            };
    [NetworkHandle postRequestForApi:@"auth/setPasswordStep2" mockObj:nil params:para handle:^NSArray *(id response) {
        
        return @[@YES];
        
    } success:^(id response){
        if (succBlock) {
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)loginWithPhone:(NSString *)phone
              password:(NSString *)pwd_
            verifyCode:(NSString *)code
               success:(RequestSuccessBlock)succBlock
               failure:(RequestFailureBlock)failBlock{
 
    NSDictionary * para = @{@"cellphone":phone,
                            @"password":pwd_,
                            @"verificationCode":code,
                            @"plateformType":@"PLATFORM_MARKECTER"
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"auth/login" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * user = response_[@"user"];
        if (user) {
            SellerModel * model = [SellerModel modelWithDictionary:user];
            model.networkToken = token;
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curSellerModel = model;
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getSMSVerifyCodeWithPhone:(NSString *)phone
                             type:(NSString *)type
                          success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBloc{
    NSDictionary * dic = @{@"cellphone":phone,@"type":type,@"platformType":@"PLATFORM_MARKECTER"};
    [NetWorkHelper postRequestForApi:@"auth/sendCode" params:dic handle:^NSArray *(id response) {
        NSLog(@"code...%@",response);
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}


@end


