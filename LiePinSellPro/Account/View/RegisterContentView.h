//
//  RegisterContentView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterContentViewDelegate
//  注册  phone:登录手机号   password:登录密码    verifyCode:登录验证码
- (void)registerWithCellphone:(NSString *)phone password:(NSString *)password verifyCode:(NSString *)verifyCode;
//  返回登录页面
- (void)backToLogin;
// 获取验证码
- (void)getVerifyCode:(NSString *)phone;
@end

@interface RegisterContentView : UIView

@property (nonatomic , weak) id<RegisterContentViewDelegate>delegate;

- (void)getVerifyCodeSuccess;

@end
