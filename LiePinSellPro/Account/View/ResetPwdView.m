//
//  ResetPwdView.m
//  LiePinSellPro
//
//  Created by Deve on 2018/3/1.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ResetPwdView.h"
#import "UIImage+YYAdd.h"
#import "FlyTextFile.h"

@interface ResetPwdView()<UITextFieldDelegate,FlyTextFileDelegate>{
    
    FlyTextFile * phoneTf;         //  密码
    FlyTextFile * passwordTf;      //  再次确认密码
    UIButton * suereButton;       //  确定按钮
}
@end


@implementation ResetPwdView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    @weakify(self)
    UIImageView * bkImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginAccountBk"]];
    [self addSubview:bkImgv];
    [bkImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(-HorPxFit(50));
        make.right.equalTo(self).mas_offset(HorPxFit(50));
        make.top.equalTo(self).mas_offset(-VerPxFit(33));
        make.bottom.equalTo(self).mas_offset(VerPxFit(50));
    }];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(bkAction) forControlEvents:UIControlEventTouchUpInside];
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self addSubview:bkBt];
    [bkBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(HorPxFit(10));
        make.top.equalTo(self).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(40), VerPxFit(40)));
    }];
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(100), VerPxFit(50))];
    titleLb.text = @"重置密码";
    titleLb.font = [UIFont systemFontOfSize:21];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), VerPxFit(46)));
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    CGFloat cellH = VerPxFit(40);
    CGFloat verPadding = VerPxFit(20);
    CGFloat horPadding = HorPxFit(120);
    
    phoneTf = [self tf:@"请输入至少8位有效密码"];
    phoneTf.secureTextEntry = YES;
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    phoneTf.tag = 100;
    [self addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(30));
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(cellH);
    }];
    
    passwordTf = [self tf:@"确认密码"];
    passwordTf.secureTextEntry = YES;
    passwordTf.tag = 101;
    [self addSubview:passwordTf];
    [passwordTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(phoneTf);
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verPadding);
        make.left.equalTo(phoneTf);
        make.height.mas_equalTo(cellH);
    }];
    
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"registerBk"] forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:18];
    sureBt.layer.cornerRadius = 5;
    [self addSubview:sureBt];
    
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.bottom.equalTo(self).mas_offset(-VerPxFit(40));
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(HorPxFit(280));
        make.height.mas_equalTo(VerPxFit(40));
    }];
}

- (FlyTextFile *)tf:(NSString *)placeHolder{
    FlyTextFile * tf = [[FlyTextFile alloc] init];
    tf.titleLbH = 16;
    [tf createSubViews];
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    tf.Titlefont = [UIFont systemFontOfSize:13];
    tf.placeholder = placeHolder;
    
    return tf;
}


- (void)bkAction{
 
     [self.delegate resetPwdBackTofind];
}

- (void)sureAction{
    [self resignFirstResponder];
    if (!phoneTf.text ||  phoneTf.text.length ==0) {
        [BaseHelper showProgressHud:@"请输入新密码" showLoading:NO canHide:YES];
        return;
    }
    
    if (!passwordTf.text  || passwordTf.text.length ==0 ) {
        [BaseHelper showProgressHud:@"请输入新密码" showLoading:NO canHide:YES];
        return;
    }
    
    if (![phoneTf.text isEqualToString:passwordTf.text]) {
        [BaseHelper showProgressHud:@"两次密码输入不一致" showLoading:NO canHide:YES];
        return;
    }
    if(![BaseHelper passwordlength:phoneTf.text]){
        [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
        return;
    }
    
    [self.delegate resetPwdWithNewPwd:phoneTf.text];

}

#pragma mark -- UITextFieldDelegate
- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}
@end
