//
//  LoginContentView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginContentViewDelegate
/*
 *  登录  phone 用户名   password  密码
 */
- (void)loginWithPhone:(NSString *)phone password:(NSString *)password;
//  注册
- (void)registerAction;
// 找回密码
- (void)forgetPassword;
@end

@interface LoginContentView : UIView

@property (nonatomic , weak) id<LoginContentViewDelegate>delegate;
@property (nonatomic , assign) BOOL isAutoLogin;   // 是否自动登录

@end

