//
//  LoginContentView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginContentView.h"
#import "FlyTextFile.h"

#define CellH HorPxFit(40)
#define InputVerPadding VerPxFit(20)
#define AutoLoginImgVTag  1100
#define LoginTypeSelColor [UIColor whiteColor]
#define LoginTypeUnselColor  COLOR(220, 220, 220, 1)

@interface LoginContentView()<FlyTextFileDelegate>{
   
    FlyTextFile * phoneTfAc;   //  登录用户名
    FlyTextFile * pwdTfAC;      // 登录密码
    FlyTextFile * vercodeTfAc;   // 登录验证码
    UIImageView * verCodeImgAc;   // 验证码图片
    UIButton * pwdFlagV;         // 密码隐显按钮
    UIButton * autoLoginBt;     // 自动登录按钮
}
@end


@implementation LoginContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UIImageView * bkImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginAccountBk"]];
    [self addSubview:bkImgv];
    @weakify(self)
    [bkImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(-HorPxFit(50));
        make.right.equalTo(self).mas_offset(HorPxFit(50));
        make.top.equalTo(self).mas_offset(-VerPxFit(33));
        make.bottom.equalTo(self).mas_offset(VerPxFit(50));
    }];
    
    CGFloat horPadding = HorPxFit(120);
    CGFloat verPadding = VerPxFit(20);
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(100), VerPxFit(50))];
    titleLb.text = @"登录";
    titleLb.font = [UIFont systemFontOfSize:21];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(46)));
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
  
    phoneTfAc = [self tf:@"手机号"];
    phoneTfAc.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:phoneTfAc];
    [phoneTfAc mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(0);
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(CellH);
    }];
    
    pwdTfAC = [self tf:@"密码"];
    pwdTfAC.secureTextEntry = YES;
    pwdTfAC.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:pwdTfAC];
    [pwdTfAC mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(phoneTfAc.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(CellH);
    }];
    
    pwdFlagV = [ UIButton buttonWithType:UIButtonTypeCustom];
    [pwdFlagV addTarget:self action:@selector(pwdSecureAction) forControlEvents:UIControlEventTouchUpInside];
    [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
    [self addSubview:pwdFlagV];
    CGFloat h  = VerPxFit(20);
    CGFloat w = h*160/120;
    [pwdFlagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(pwdTfAC);
        make.centerY.mas_equalTo(pwdTfAC.mas_centerY).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(w, h));
    }];
    
    verCodeImgAc = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"verfyCode"]];
    [self addSubview:verCodeImgAc];
    [verCodeImgAc mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).mas_offset(-horPadding);
        make.top.mas_equalTo(pwdTfAC.mas_bottom).mas_offset(InputVerPadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), CellH));
    }];
    
    vercodeTfAc = [self tf:@"验证码"];
    [self addSubview:vercodeTfAc];
    [vercodeTfAc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(pwdTfAC);
        make.top.mas_equalTo(pwdTfAC.mas_bottom).mas_offset(InputVerPadding);
        make.right.mas_equalTo(verCodeImgAc.mas_left).mas_offset(-HorPxFit(10));
        make.height.mas_equalTo(CellH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:16];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"loginbtBk"] forState:UIControlStateNormal];
    sureBt.layer.cornerRadius = 6;
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBt addTarget:self action:@selector(suerAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.mas_equalTo(self.mas_bottom).mas_offset(-VerPxFit(90));
        make.width.mas_equalTo(HorPxFit(280));
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.mas_equalTo(VerPxFit(40));
    }];
    
    UIImageView * autologinImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FuXuan_sel"]];
    autologinImgV.tag = AutoLoginImgVTag;
    [self addSubview:autologinImgV];
    [autologinImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sureBt);
        make.top.mas_equalTo(sureBt.mas_bottom).mas_offset(VerPxFit(18));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(20), HorPxFit(20)));
    }];
    UILabel * autoLogin = [[UILabel alloc] init];
    autoLogin.font = [UIFont systemFontOfSize:15];
    autoLogin.text = @"自动登录";
    autoLogin.textColor = [UIColor whiteColor];
    [self addSubview:autoLogin];
    [autoLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(autologinImgV.mas_right);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    autoLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [autoLoginBt addTarget:self action:@selector(autologinShift:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:autoLoginBt];
    [autoLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(autologinImgV);
        make.right.equalTo(autoLogin);
        make.height.mas_equalTo(VerPxFit(40));
        make.centerY.equalTo(autologinImgV.mas_centerY);
    }];
    id obj = [DataDefault objectForKey:DataDefaultAutologinKey isPrivate:NO];
    BOOL isAutoLogin = NO;
    if (!obj) {
        [DataDefault setObject:@(YES) forKey:DataDefaultAutologinKey isPrivate:NO];
        isAutoLogin = YES;
    }else{
        isAutoLogin = [obj boolValue];
    }
    if (isAutoLogin) {
        autoLoginBt.selected = YES;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autoLoginBt.selected = NO;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    
    
    UIButton * forgetPwdBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetPwdBt setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetPwdBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [forgetPwdBt addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    forgetPwdBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:forgetPwdBt];
    [forgetPwdBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(sureBt);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100, VerPxFit(40)));
    }];
    
    UIButton * registerBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerBt setTitle:@"注册" forState:UIControlStateNormal];
    [registerBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerBt addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
    registerBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:registerBt];
    [registerBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(forgetPwdBt.mas_top);
        make.centerX.mas_equalTo(titleLb.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), VerPxFit(40)));
    }];
}

- (BOOL)isAutoLogin{
    return  autoLoginBt.selected;
}

- (void)autologinShift:(UIButton *)bt{
    UIImageView * autologinImgV = [self viewWithTag:AutoLoginImgVTag];
    bt.selected = !bt.selected;
    if (bt.selected) {
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    [DataDefault setObject:@(bt.selected) forKey:DataDefaultAutologinKey isPrivate:NO];
}

- (void)suerAction{
    [self resignFirstResponder];
    if (!phoneTfAc.text || phoneTfAc.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入手机号" showLoading:NO canHide:YES];
        return;
    }
    if (!pwdTfAC.text || pwdTfAC.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入登录密码" showLoading:NO canHide:YES];
        return;
    }
    if(![BaseHelper passwordlength:pwdTfAC.text]){
        [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
        return;
    }
    
    [self.delegate loginWithPhone:phoneTfAc.text password:pwdTfAC.text];
}

- (void)pwdSecureAction{
    if (pwdTfAC.secureTextEntry) {
        pwdTfAC.secureTextEntry = NO;
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeOpen"] forState:UIControlStateNormal];
    }else{
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
        pwdTfAC.secureTextEntry = YES;
    }
}

- (void)registerAction{
    [self.delegate registerAction];
}

- (void)forgetPwdAction{
    [self.delegate forgetPassword];
}

- (FlyTextFile *)tf:(NSString *)placeHolder{
    FlyTextFile * tf = [[FlyTextFile alloc] init];
    tf.titleLbH = 20;
    [tf createSubViews];
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    tf.Titlefont = [UIFont systemFontOfSize:13];
    tf.placeholder = placeHolder;
    return tf;
}

#pragma mark -- FlyTextFileDelegate
- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * validString = @"0123456789";
    if (textField == phoneTfAc) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(FlyTextFile*)textField{
    return YES;
}

@end
