//
//  SellerModel.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/8.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellerModel : NSObject

@property (nonatomic , assign) NSInteger id_;           //  id
@property (nonatomic , strong) NSString * cellphone;    // 手机号
@property (nonatomic , assign) NSInteger score;         // 评分
@property (nonatomic , assign) NSInteger evaluateCount; //  被评价次数
@property (nonatomic , strong) NSString * headPic;      // 头像URL
@property (nonatomic , strong) NSString * nickName;     // 昵称

@property (nonatomic , strong) NSString * networkToken;   //  网络访问token，自动等候后从该处获取

@end
