//
//  LangeSetViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "LangeSetViewController.h"

@interface LangeSetViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITableView * tableView;
}

@property (nonatomic , assign) NSDictionary * curLange;
@property (nonatomic , strong) NSArray * langeList;

@end

@implementation LangeSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"语言设置";
    self.view.backgroundColor = [UIColor whiteColor];
    self.langeList = @[@{@"lange":@"中文",@"id":@(1)},@{@"lange":@"English",@"id":@(2)}];
    
    NSDictionary * curLange = [DataDefault objectForKey:@"CurLanage" isPrivate:NO];
    if (!curLange) {
        self.curLange = self.langeList[0];
    }else{
        for (NSDictionary * langeDic in self.langeList) {
            NSInteger curId = [curLange[@"id"] integerValue];
            NSInteger langeId = [langeDic[@"id"] integerValue];
            if (curId == langeId) {
                self.curLange = langeDic;
                break;
            }
        }
    }
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).mas_offset(VerPxFit(NavigationBarHeight));
        make.left.and.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    [BaseHelper setExtraCellLineHidden:tableView];
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(langeSetFinish)];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)langeSetFinish{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _langeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font  = [UIFont systemFontOfSize:15];
    }
    NSDictionary * dic = _langeList[indexPath.row];
    cell.textLabel.text = dic[@"lange"];
    
    NSInteger id_ = [dic[@"id"] integerValue];
    NSInteger curLangeId = [_curLange[@"id"] integerValue];
    if (id_ == curLangeId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * dic = _langeList[indexPath.row];
    
    NSInteger id_ = [dic[@"id"] integerValue];
    NSInteger curLangeId = [_curLange[@"id"] integerValue];
    
    if (id_ == curLangeId) {
        return;
    }else{
        NSInteger index = [_langeList indexOfObject:_curLange];
        UITableViewCell * cell  = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        UITableViewCell * cell_ = [tableView cellForRowAtIndexPath:indexPath];
        cell_.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    self.curLange = dic;
}

@end

