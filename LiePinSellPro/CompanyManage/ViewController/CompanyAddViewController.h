//
//  CompanyAddViewController.h
//  LiePinSellPro
//
//  Created by Michael on 2018/2/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"

@protocol CompanyAddViewControllerDelegate

- (void)companyListNeedUpdate;

@end

@interface CompanyAddViewController : UIViewController

@property (nonatomic , strong) CompanyModel * companyModel;
@property (nonatomic , assign) id<CompanyAddViewControllerDelegate>delegate;


@end
