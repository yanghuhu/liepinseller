//
//  ScanViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "ScanViewController.h"
#import "QRCodeScanManage.h"
#import "UIImage+YYAdd.h"
#import "UIView+YYAdd.h"

@interface ScanViewController ()<QRCodeScanManageDelegate,UIAlertViewDelegate>

@property (nonatomic , strong) QRCodeScanManage * qRCodeScanManage;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"识别二维码";
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    bkBt.frame = CGRectMake(0, VerPxFit(25), 45, 45);
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.view addSubview:bkBt];
    UIBarButtonItem * item  = [[UIBarButtonItem alloc] initWithCustomView:bkBt];
    self.navigationItem.leftBarButtonItem = item;
}

- (void)backAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.qRCodeScanManage initSet];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    lb.centerY = self.view.centerY;
    lb.textColor = [UIColor whiteColor];
    lb.text = @"请将二维码对准扫描框";
    lb.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lb];
}

- (QRCodeScanManage *)qRCodeScanManage{
    if (!_qRCodeScanManage) {
        _qRCodeScanManage = [[QRCodeScanManage alloc] init];
        _qRCodeScanManage.supView = self.view;
        _qRCodeScanManage.delegate = self;
    }
    return _qRCodeScanManage;
}

#pragma mark -- QRCodeScanManageDelegate
- (void)qrScanSuccess:(NSString *)qrInfo{
//  获取到网页url，调用解析库对网页解析，获取相应字段信息
}


@end
