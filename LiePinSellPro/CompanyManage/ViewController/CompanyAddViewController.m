//
//  CompanyAddViewController.m
//  LiePinSellPro
//
//  Created by Michael on 2018/2/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "CompanyAddViewController.h"
#import "AccountView.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SettingView.h"
#import "HomeModule.h"
#import "CompanyModel.h"
#import "ScanViewController.h"
#import "NetWorkHelper.h"
#import "UIImage+YYAdd.h"

#define BaseTag  100

@interface CompanyAddViewController ()<UITextFieldDelegate,AccountViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,SettingViewDelegate,UIAlertViewDelegate>{
    
    UIView * contentView;
    
    UITextField * companyNameTf;    //  企业名称
    UITextField * shuihaoTf;         //  税号
    UITextField * personTf;         // 联系人
    UITextField * phoneTf;            //  联系方式
    UITextField * addressTf;          //  地址
    UIImageView * yingyezhizhao;       // 添加营业执照按钮
    UIButton * manageAddBt;            // 管理员添加弹框按钮
    
    BOOL isUpdataCompanyInfo;           //  是否为更新企业信息
}

@property (nonatomic , strong) UIImagePickerController * imagePickerController;

@property (nonatomic , strong) AccountView * accountView;       // 管理员弹窗
@property (nonatomic , strong) SettingView * settingView;       // 企业配置弹窗
@property (nonatomic , strong) UIImage * yingyezhizhao;         // 营业执照图片
@property (nonatomic , strong) UIButton * rechargeBt;           // 充值按钮
@property (nonatomic , strong) NSString * orderNum;             // 充值订单号

@end

@implementation CompanyAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    if (_companyModel) {
        isUpdataCompanyInfo = YES;
    }else{
        isUpdataCompanyInfo = NO;
    }
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionHomeBk"]];
    [self.view addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self.view);
    }];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    bkBt.frame = CGRectMake(HorPxFit(25), VerPxFit(25), 60, 45);
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.view addSubview:bkBt];
    
    UIButton * scanBt = [UIButton buttonWithType:UIButtonTypeCustom];
    scanBt.frame = CGRectMake(ScreenWidth - HorPxFit(120)-HorPxFit(40), VerPxFit(30), HorPxFit(120), VerPxFit(86));
    [scanBt addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [scanBt setImage:[UIImage imageNamed:@"scan"] forState:UIControlStateNormal];
    [self.view addSubview:scanBt];
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(80), VerPxFit(130), ScreenWidth-HorPxFit(80)*2, ScreenHeight-VerPxFit(190))];
    contentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentView];
    
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"posiontHomeCotentBK"]];
    contentBk.frame = CGRectMake(-HorPxFit(20), -VerPxFit(10), CGRectGetWidth(contentView.frame)+2*HorPxFit(20), CGRectGetHeight(contentView.frame)+VerPxFit(10)+VerPxFit(26));
    [contentView addSubview:contentBk];
    
    CGFloat horPadding = HorPxFit(50);
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(horPadding, VerPxFit(15), 200, 25)];
    titleLb.text = @"企业信息";
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [contentView addSubview:titleLb];
    
    UIImageView * lineImgv = [[UIImageView alloc] init];
    lineImgv.image = [UIImage imageNamed:@"bkLine"];
    [contentView addSubview:lineImgv];
    [lineImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(10));
        make.right.equalTo(contentView);
        make.height.mas_equalTo(2);
    }];
    
    if (_companyModel && !_companyModel.hasHRManager) {
        manageAddBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [manageAddBt setTitle:@"添加管理员" forState:UIControlStateNormal];
        [manageAddBt addTarget:self action:@selector(addCompanyManage) forControlEvents:UIControlEventTouchUpInside];
        manageAddBt.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [manageAddBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contentView addSubview:manageAddBt];
        [manageAddBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(contentView).mas_offset(-HorPxFit(20));
            make.centerY.equalTo(titleLb.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(100, VerPxFit(45)));
        }];
    }
    
    if (_companyModel && _companyModel.hasHRManager) {
        self.rechargeBt.hidden = NO;
    }
    
    [self addInputFile];
    if (_companyModel) {
        companyNameTf.text = _companyModel.name;
        shuihaoTf.text = _companyModel.taxNumber;
        phoneTf.text = _companyModel.contactNumber;
        personTf.text = _companyModel.contacts;
        addressTf.text =  _companyModel.address;
    }
}

- (void)addCompanyManage{
    [self.accountView showWithSupView:self.view];
}

- (UIButton *)rechargeBt{
    if (!_rechargeBt) {
        _rechargeBt = [UIButton buttonWithType:UIButtonTypeCustom];
        _rechargeBt.hidden = YES;
        [_rechargeBt setTitle:@"充值" forState:UIControlStateNormal];
        [_rechargeBt addTarget:self action:@selector(rechargeAction) forControlEvents:UIControlEventTouchUpInside];
        _rechargeBt.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [_rechargeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contentView addSubview:_rechargeBt];
        [_rechargeBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(contentView).mas_offset(-HorPxFit(20));
            make.top.equalTo(contentView).mas_offset(VerPxFit(5));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(90), VerPxFit(45)));
        }];
    }
    return _rechargeBt;
}

- (void)addInputFile{
    
    CGFloat horpadding = HorPxFit(70);
    CGFloat horInterVer = HorPxFit(20);
    CGFloat verpadding = VerPxFit(40);
    CGFloat inputW = HorPxFit(300);
    CGFloat titleW = HorPxFit(90);
    CGFloat cellH = VerPxFit(30);
    
    CGFloat curY = VerPxFit(15)+25+VerPxFit(10)+30+VerPxFit(40);
    
    UILabel * companyNametitle = [self lb:@"企业名称:"];
    [companyNametitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).mas_offset(horpadding);
        make.top.mas_equalTo(curY);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    companyNameTf = [self tfWithTag:BaseTag];
    [[companyNameTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(companyNametitle.mas_right).mas_offset(horInterVer);
        make.top.and.height.equalTo(companyNametitle);
        make.width.mas_equalTo(inputW);
    }];
    
    UILabel * shuihaoTitle = [self lb:@"税号:"];
    [shuihaoTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companyNametitle.mas_bottom).mas_offset(verpadding);
        make.left.equalTo(companyNametitle);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    shuihaoTf = [self tfWithTag:BaseTag+1];
    [[shuihaoTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(shuihaoTitle.mas_right).mas_offset(horInterVer);
        make.top.and.height.equalTo(shuihaoTitle);
        make.width.mas_equalTo(inputW);
    }];
    
    UILabel * personTitle = [self lb:@"联系人:"];
    [personTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(shuihaoTitle.mas_bottom).mas_offset(verpadding);
        make.left.equalTo(companyNametitle);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    personTf = [self tfWithTag:BaseTag+2];
    [[personTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(personTitle.mas_right).mas_offset(horInterVer);
        make.top.and.height.equalTo(personTitle);
        make.width.mas_equalTo(inputW);
    }];
    
    UILabel * phoneTitle = [self lb:@"电话:"];
    [phoneTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(personTitle.mas_bottom).mas_offset(verpadding);
        make.left.equalTo(companyNametitle);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    phoneTf = [self tfWithTag:BaseTag+3];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [[phoneTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneTitle.mas_right).mas_offset(horInterVer);
        make.top.and.height.equalTo(phoneTitle);
        make.width.mas_equalTo(inputW);
    }];
    
    UILabel * addressTitle = [self lb:@"办公地址:"];
    [addressTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(phoneTitle.mas_bottom).mas_offset(verpadding);
        make.left.equalTo(companyNametitle);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    addressTf = [self tfWithTag:BaseTag+4];
    addressTf.returnKeyType = UIReturnKeyDone;
    [[addressTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(addressTitle.mas_right).mas_offset(horInterVer);
        make.top.and.height.equalTo(addressTitle);
        make.width.mas_equalTo(inputW);
    }];
    
    yingyezhizhao = [UIImageView new];
    yingyezhizhao.image = [UIImage imageNamed:@"yingyezhizhao"];
    [contentView addSubview:yingyezhizhao];
    [yingyezhizhao mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companyNametitle.mas_top);
        make.left.mas_equalTo([addressTf superview].mas_right).mas_offset(HorPxFit(100));
        make.width.mas_equalTo(@(HorPxFit(210)));
        make.height.mas_equalTo(@(VerPxFit(280)));
    }];
    
    
    UIButton *uploadYingyezhizhaoBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [uploadYingyezhizhaoBt setImage:[UIImage imageNamed:@"potoImage"] forState:UIControlStateNormal];
    [uploadYingyezhizhaoBt setTitle:@"点击上传照片" forState:UIControlStateNormal];
    uploadYingyezhizhaoBt.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    uploadYingyezhizhaoBt.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    [uploadYingyezhizhaoBt addTarget:self action:@selector(addYingYezhizhao) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:uploadYingyezhizhaoBt];
    [uploadYingyezhizhaoBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(yingyezhizhao.mas_bottom).offset(5);
        make.left.mas_equalTo(yingyezhizhao).offset(20);
        make.right.mas_equalTo(yingyezhizhao).offset(-20);
        make.height.equalTo(@(60));
    }];
    
    
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setTitle:@"确 定" forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(companyInfoSubmitAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setBackgroundImage:[UIImage imageNamed:@"sureBtBk"] forState:UIControlStateNormal];
    [contentView addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contentView.mas_centerX);
        make.bottom.mas_equalTo(contentView).mas_offset(-VerPxFit(40));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(300), VerPxFit(40)));
    }];
}

- (void)rechargeAction{
    [self.settingView showWithSupView:self.view];
}

- (void)companyInfoSubmitAction{
    
    //    if (!companyNameTf.text || companyNameTf.text.length == 0) {
    //        [BaseHelper showProgressHud:@"请添加企业名称" showLoading:NO canHide:YES];
    //        return;
    //    }else if (!shuihaoTf.text || shuihaoTf.text.length == 0){
    //        [BaseHelper showProgressHud:@"请添加企业税号" showLoading:NO canHide:YES];
    //        return;
    //    }else if (!personTf.text || personTf.text.length == 0){
    //        [BaseHelper showProgressHud:@"请添加联系人" showLoading:NO canHide:YES];
    //        return;
    //    }else if (!phoneTf.text || phoneTf.text.length == 0 || ![BaseHelper isValidateMobile:phoneTf.text]){
    //        [BaseHelper showProgressHud:@"请添加有效联系方式" showLoading:NO canHide:YES];
    //        return;
    //    }else if (!addressTf.text || addressTf.text.length == 0){
    //        [BaseHelper showProgressHud:@"请添加企业办公地址" showLoading:NO canHide:YES];
    //        return;
    //    }
    //    else if (!_yingyezhizhao){
    //        [BaseHelper showProgressHud:@"请添加企业营业执照" showLoading:NO canHide:YES];
    //        return;
    //    }
    //
    
    NSString * licenseUrl = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1515995710518&di=b6407e2e3f5e3fba057fa85e30967bc5&imgtype=0&src=http%3A%2F%2Fimg1.bmlink.com%2Fbig%2Fsupply%2F2015%2F3%2F18%2F9%2F291152934679367.jpg";
    
    [BaseHelper showProgressLoadingInView:self.view];
    //  更新公司信息
    if(_companyModel){
        [HomeModule updateCompanyInfoWithId:_companyModel.id_ ContactNumber:phoneTf.text contacts:personTf.text companyName:companyNameTf.text taxNumber:shuihaoTf.text businessLicense:licenseUrl address:addressTf.text success:^(CompanyModel * companyModel){
            self.companyModel = companyModel;
            [BaseHelper hideProgressHudInView:self.view];
            [self.delegate companyListNeedUpdate];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
        }];
    }else{    // 提交公司信息
        [HomeModule submitCompanyInfoWithContactNumber:phoneTf.text contacts:personTf.text companyName:companyNameTf.text taxNumber:shuihaoTf.text businessLicense:licenseUrl address:addressTf.text success:^(CompanyModel * companyModel){
            self.companyModel = companyModel;
            [BaseHelper hideProgressHudInView:self.view];
            [self.accountView showWithSupView:self.view];
            [self.delegate companyListNeedUpdate];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
        }];
    }
}

- (AccountView *)accountView{
    if (!_accountView) {
        _accountView = [[AccountView alloc] init];
        _accountView.delegate = self;
    }
    return _accountView;
}

- (SettingView *)settingView{
    if (!_settingView) {
        _settingView = [[SettingView alloc] init];
        _settingView.delegate = self;
    }
    return _settingView;
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentRight;
    lb.text = text;
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:17];
    [contentView addSubview:lb];
    return  lb;
}

- (UITextField *)tfWithTag:(NSInteger)tag{
    UIView * view = [[UIView alloc] init];
    [contentView addSubview:view];
    
    UIImageView * line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(view);
        make.height.mas_equalTo(1.5);
    }];
    
    UITextField * tf = [[UITextField alloc] init];
    tf.tag  = tag;
    tf.textColor = [UIColor whiteColor];
    tf.returnKeyType = UIReturnKeyNext;
    tf.delegate = self;
    tf.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.equalTo(view);
        make.bottom.mas_equalTo(line.mas_top);
    }];
    
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    tf.leftView = leftView;
    return tf;
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)addYingYezhizhao{
    @weakify(self)
    UIAlertController * alert  =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * camerAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self selectImageFromCamera];
    }];
    UIAlertAction * photoesAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self selectImageFromAlbum];
    }];
    UIAlertAction * cancelAction =  [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:camerAction];
    [alert addAction:photoesAction];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = yingyezhizhao;
    popPresenter.sourceRect = yingyezhizhao.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)scanAction{
    ScanViewController * vc = [[ScanViewController alloc] init];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nav animated:YES completion:^{
    }];
}

- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        _imagePickerController.allowsEditing = YES;
    }
    return _imagePickerController;
}

- (void)selectImageFromAlbum
{
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [HSBCGlobalInstance sharedHSBCGlobalInstance].isShowLibrary = YES;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}

- (void)selectImageFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagePickerController animated:YES completion:nil];
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"无法获取到相机设备" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)clearInputInfo{
    companyNameTf.text = @"";
    shuihaoTf.text=@"";
    personTf.text = @"";
    phoneTf.text = @"";
    addressTf.text = @"";
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag == BaseTag+4) {
        [textField resignFirstResponder];
    }else{
        UITextField * tf = [contentView viewWithTag:textField.tag+1];
        if (tf) {
            [tf becomeFirstResponder];
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == phoneTf) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        NSString * validString = @"0123456789";
        if ([validString rangeOfString:string].location != NSNotFound) {
            return YES;
            
        }else if ([string isEqualToString:@""]){
            return YES;
            
        }
        return NO;
    }
    return YES;
}

#pragma mark -- AccountViewDelegate
- (void)accountSubmitWithIdCard:(NSString *)idCard jobNum:(NSString *)jobNum account:(NSString *)account password:(NSString *)password{
    
    [BaseHelper showProgressLoadingInView:self.view];
    [HomeModule submitManageAccountWithUserName:account idCardString:idCard jobNum:jobNum companyId:_companyModel.id_ password:password success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"管理员账号创建成功" showLoading:NO canHide:YES];
        [self.accountView dismiss];
        [self.settingView showWithSupView:self.view];
        if (manageAddBt && [manageAddBt superview]) {
            manageAddBt.hidden = YES;
            self.rechargeBt.hidden = NO;;
        }
        [self.delegate companyListNeedUpdate];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

#pragma mark -- SettingViewDelegate
- (void)settingFinishWithType:(NSString *)type{
    
    [BaseHelper showProgressLoadingInView:self.view];
    [HomeModule submitCompanyPayTypeWithId:_companyModel.id_ type:type success:^{
        [BaseHelper hideProgressHudInView:self.view];
        if ([type isEqualToString:@"ORDER_SETTLE"]) {
            if (!isUpdataCompanyInfo) {
                [self clearInputInfo];
            }
            [BaseHelper showProgressHud:@"企业配置成功" showLoading:NO canHide:YES];
            if ([self.companyModel.state isEqualToString:@"ON"]) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"订单号" message:self.orderNum delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                alert.tag = 101;
                [alert show];
            }
            [self.settingView dismiss];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self moneyRechargeOrderCreate];
        }
        [self.delegate companyListNeedUpdate];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}
// 生成充值订单
- (void)moneyRechargeOrderCreate{
    [BaseHelper showProgressLoadingInView:self.view];
    [HomeModule compayRechargeOrderCrateWihtId:_companyModel.id_ Money:self.settingView.money*100 success:^(NSString * orderNum){
        [BaseHelper hideProgressHudInView:self.view];
        self.orderNum = orderNum;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"订单号" message:self.orderNum delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        alert.tag = 100;
        [alert show];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}
// 确认订单
- (void)moenyOrderSure{
    [BaseHelper showProgressLoadingInView:self.view];
    [HomeModule compayRechargeOrderSure:_orderNum success:^{
        [BaseHelper hideProgressHudInView:self.view];
        if (!isUpdataCompanyInfo) {
            [self clearInputInfo];
        }
        [self.settingView dismiss];
        [BaseHelper showProgressHud:@"企业配置充值成功" showLoading:NO canHide:YES];
        [self.delegate companyListNeedUpdate];
        if ([self.companyModel.state isEqualToString:@"OFF"]) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"您的企业已经添加成功，请在企业列表启动该企业" delegate:self cancelButtonTitle:nil otherButtonTitles:@"好的", nil];
            alert.tag = 102;
            [alert show];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    NSString *mediaType=[info objectForKey:UIImagePickerControllerMediaType];
    //判断资源类型
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        UIImage * img_ = info[UIImagePickerControllerEditedImage];
        self.yingyezhizhao = img_;
        yingyezhizhao.image = img_;
    }
    [HSBCGlobalInstance sharedHSBCGlobalInstance].isShowLibrary = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case 101:
        {
            if (buttonIndex == 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;
        case 100:{
            if (buttonIndex == 1) {
                [self moenyOrderSure];
            }
        }
            break;
        case 102:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        default:
            break;
    }
}

@end

