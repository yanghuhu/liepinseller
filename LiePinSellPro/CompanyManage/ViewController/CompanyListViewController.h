//
//  CompanyListViewController.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface CompanyListViewController : BaseViewController

//  更新企业列表
- (void)updateCompanyListData;

@end
