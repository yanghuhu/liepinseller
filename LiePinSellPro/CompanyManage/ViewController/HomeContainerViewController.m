//
//  HomeContainerViewController.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "HomeContainerViewController.h"
#import "CompanyListViewController.h"
#import "CompanyAddViewController.h"
#import "CompanyAddViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "UIImage+YYAdd.h"
#import "UserCenterViewController.h"
#import "BaseNavigationController.h"

@interface HomeContainerViewController ()<CompanyAddViewControllerDelegate>{
    UIButton * companyListBt;
    UIImageView * selFlag;
}

@property (nonatomic , strong) CompanyListViewController * compListViewController;  // 企业列表VC
@property (nonatomic , strong) CompanyAddViewController * compAddViewController;   // 企业添加VC

@end

@implementation HomeContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [self createSubViews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toAddCompanyViewController:) name:TOVIEWCONTROLLER_ADDCOMPANY object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(245), ScreenHeight)];
    [self.view addSubview:leftView];
    
    UIImageView * bkImag = [[UIImageView alloc] initWithFrame:leftView.bounds];
    bkImag.image = [UIImage imageNamed:@"leftBk"];
    [leftView addSubview:bkImag];
    
    UIButton * userHomeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [userHomeBt addTarget:self action:@selector(toUserHomeVCAction) forControlEvents:UIControlEventTouchUpInside];
    [userHomeBt setImage:[[UIImage imageNamed:@"UserHome"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    userHomeBt.titleLabel.font = [UIFont systemFontOfSize:20];
    [userHomeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    userHomeBt.frame = CGRectMake(HorPxFit(15), VerPxFit(40), 100, 40);
    [self.view addSubview:userHomeBt];
    
    [userHomeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(40));
        make.top.mas_equalTo(VerPxFit(40));
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
    
    companyListBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [companyListBt addTarget:self action:@selector(rightViewShift:) forControlEvents:UIControlEventTouchUpInside];
    [companyListBt setTitle:@"企业列表" forState:UIControlStateNormal];
    [companyListBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftView addSubview:companyListBt];
    [companyListBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(leftView.mas_centerY).mas_offset(-VerPxFit(10));
        make.centerX.mas_equalTo(leftView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(150), VerPxFit(50)));
    }];
 
    selFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"segmentBk"]];
    [leftView addSubview:selFlag];
    [selFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companyListBt.mas_bottom);
        make.centerX.mas_equalTo(leftView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(133), VerPxFit(5)));
    }];
    
    [self addChildViewController:self.compListViewController];
    [self.view addSubview:self.compListViewController.view];
}

- (void)rightViewShift:(UIButton *)bt{
    [_compListViewController.view removeFromSuperview];
    [_compListViewController removeFromParentViewController];
    [_compAddViewController.view removeFromSuperview];
    [_compAddViewController removeFromParentViewController];
    if (bt == companyListBt) {
        [self addChildViewController:self.compListViewController];
        [self.view addSubview:self.compListViewController.view];
    }else{
        [self addChildViewController:self.compAddViewController];
        [self.view addSubview:self.compAddViewController.view];
    }
}

- (void)toUserHomeVCAction{
    UserCenterViewController * vc = [[UserCenterViewController alloc] init];
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nav animated:YES completion:^{}];
}

- (CompanyListViewController *)compListViewController{
    if (!_compListViewController) {
        _compListViewController = [[CompanyListViewController alloc]init];
        _compListViewController.view.frame = CGRectMake(HorPxFit(245), 0, ScreenWidth-HorPxFit(200), ScreenHeight);
    }
    return _compListViewController;
}

- (CompanyAddViewController *)compAddViewController{
    if (!_compAddViewController) {
        _compAddViewController = [[CompanyAddViewController alloc] init];
        _compAddViewController.view.frame =CGRectMake(HorPxFit(245), 0, ScreenWidth-HorPxFit(200), ScreenHeight);
    }
    return _compAddViewController;
}

- (void)toAddCompanyViewController:(NSNotification *)nofi{
    id obj= [nofi object];
    CompanyAddViewController * vc = [[CompanyAddViewController alloc] init];
    vc.companyModel = obj;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- AddCompanyViewControllerDelegate
- (void)companyListNeedUpdate{
    [_compListViewController updateCompanyListData];
}

@end
