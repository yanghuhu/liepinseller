//
//  CompanyListViewController.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "CompanyListViewController.h"
#import "CompanyListCell.h"
#import "CompanyListHeader.h"
#import "CompanyListTitleView.h"
#import "HomeModule.h"

@interface CompanyListViewController ()<UITableViewDelegate,UITableViewDataSource,CompanyListCellDelegate,CompanyListHeaderDelegate,UIAlertViewDelegate>{
    
    UITableView * tableView;
    NSInteger pageNum;         //  页面
    NSInteger searchPageNum;   //  搜索时页码
}

@property (nonatomic , assign) BOOL isSearch;            // 当前是否搜索状态
@property (nonatomic , strong) NSString * searchKey;     // 搜索关键字
@property (nonatomic , strong) NSMutableArray * companyList;         // 企业列表
@property (nonatomic , strong) NSMutableArray * companySearchList;   //  企业搜索列表

@property (nonatomic , strong) CompanyModel * curEditCompany;
@end

@implementation CompanyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR(18, 20, 26, 1);
    
    pageNum = 0;
    searchPageNum = 0;
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    CGFloat horPadding = HorPxFit(40);
    CGFloat verPadding = VerPxFit(50);
    CompanyListHeader * headerView = [[ CompanyListHeader alloc] init];
    headerView.delegate = self;
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.equalTo(self.view).mas_offset(verPadding);
        make.height.mas_equalTo(40);
    }];
    
    CompanyListTitleView * titleView = [[CompanyListTitleView alloc] init];
    [self.view addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.mas_equalTo(headerView.mas_bottom).mas_offset(verPadding/2.0);
        make.height.mas_equalTo(40);
    }];
    
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(headerView);
        make.top.mas_equalTo(titleView.mas_bottom);
        make.bottom.equalTo(self.view).mas_offset(-VerPxFit(20));
    }];
    @weakify(self)
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
    
    [BaseHelper showProgressLoadingInView:self.view];
    [self getDataSource];
}

- (void)updateCompanyListData{
    [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        if (_isSearch) {
            searchPageNum++;
        }else{
            pageNum ++;
        }
    }else{
        // 下拉刷新
        if (_isSearch) {
            searchPageNum = 0;
        }else{
            pageNum = 0;
        }
    }
    [self getDataSource];
}

- (void)getDataSource{
    if (!_companyList) {
        self.companyList = [NSMutableArray array];
    }
    if (_isSearch && !_companySearchList) {
        self.companySearchList = [NSMutableArray array];
    }
    @weakify(self)
    [HomeModule getCompanyListWithPage:_isSearch?searchPageNum:pageNum searchKey:_searchKey success:^(NSArray *array){
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        if (_isSearch) {
            if (searchPageNum == 0) {
                [_companySearchList removeAllObjects];
            }
            if (array && array.count != 0) {
                [_companySearchList addObjectsFromArray:array];
            }
        }else{
            if (pageNum == 0) {
                [_companyList removeAllObjects];
            }
            if (array && array.count != 0) {
                [_companyList addObjectsFromArray:array];
            }
        }
        [tableView reloadData];
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark --  UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_isSearch) {
        return _companySearchList.count;
    }
    return _companyList.count;
}

- (CompanyListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    CompanyListCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[CompanyListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.delegate = self;
    }
    
    CompanyModel * model = nil;
    if (_isSearch) {
        model = _companySearchList[indexPath.row];
    }else{
        model = _companyList[indexPath.row];
    }
    cell.companyModel = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -- CompanyListCellDelegate
- (void)companySwithShift:(CompanyModel *)model{
    self.curEditCompany = model;
    if ([model.state isEqualToString:@"ON"]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"确认暂停企业吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alert show];
    }else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"确认启动该企业吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
        [alert show];
    }
}

- (void)companyInfoEdit:(CompanyModel *)model{
    self.curEditCompany = model;
    [[NSNotificationCenter defaultCenter] postNotificationName:TOVIEWCONTROLLER_ADDCOMPANY object:model];
}

- (void)companySwitchNetwork{
    NSString * state = @"";
    if ([_curEditCompany.state isEqualToString:@"ON"]) {
        state = @"OFF";
    }else{
        state = @"ON";
    }
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self)
    [HomeModule swithCompnayWihtId:_curEditCompany.id_ withState:state success:^{
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        _curEditCompany.state = state;
        [tableView reloadData];
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:0];
    }];
}

#pragma mark -- CompanyListHeaderDelegate
- (void)searchWithKey:(NSString *)key{
    if (key) {
        self.searchKey = key;
        self.isSearch = YES;
        searchPageNum = 0;
        [self getDataSource];
    }else{
        self.isSearch = NO;
        [tableView reloadData];
    }
}
#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self companySwitchNetwork];
    }
}

@end
