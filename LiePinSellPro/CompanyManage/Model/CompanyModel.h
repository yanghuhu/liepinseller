//
//  CompanyModel.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/8.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSString * address;   //办公地点
@property (nonatomic , strong) NSString * businessLicense; //营业执照
@property (nonatomic , strong) NSString * contactNumber; // 联系电话
@property (nonatomic , strong) NSString * contacts;  // 联系人
@property (nonatomic , strong) NSString * name;  //公司名称
@property (nonatomic , assign)  CGFloat *percentageServiceFee;      //服务费百分比
@property (nonatomic , strong) NSString * taxNumber;    //  税号
@property (nonatomic , strong)  NSString * state;  // 状态 = ['ON', 'OFF']

@property (nonatomic , assign) BOOL hasHRManager;   //  是否已添加管理员账号

@end
