//
//  AccountView.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "AccountView.h"
#import "UIImage+YYAdd.h"
#import "UIView+YYAdd.h"

#define BaseTag 200

@interface AccountView()<UITextFieldDelegate>{
    UITextField * usernameTf;   // 用户名
    UITextField * pwdTf;        //  密码
    UITextField * jobNumTf;     //  工号
    UITextField * idCardTf;      // 身份证号
    UIButton * pwdFlagV;        //  密码显隐按钮
}

@property (nonatomic , strong) UIControl  * bkControl;

@end

@implementation AccountView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{

    CGFloat horpadding = HorPxFit(70);
    CGFloat horInterVer = HorPxFit(20);
    CGFloat verpadding = VerPxFit(20);
    CGFloat inputW = HorPxFit(350);
    CGFloat titleW = HorPxFit(90);
    CGFloat cellH = VerPxFit(30);
    
    UIImage * img = [UIImage imageNamed:@"popBack"];
    UIImageView * bkImg  = [[UIImageView alloc] initWithImage:[img stretchableImageWithLeftCapWidth:img.size.width/2.0 topCapHeight:img.size.height/2.0]];
    [self addSubview:bkImg];
    [bkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    CGFloat selfW = HorPxFit(img.size.width);
    CGFloat selfH = selfW * img.size.height / img.size.width;
    self.frame = CGRectMake(0, 0, selfW, selfH+HorPxFit(80));
    
    UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBt.frame = CGRectMake(selfW -HorPxFit(80), VerPxFit(35), 30, 30);
    [cancelBt setImage:[UIImage imageNamed:@"cancel"]  forState:UIControlStateNormal];
    [cancelBt addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBt];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"管理员账号";
    titleLb.font = [UIFont systemFontOfSize:21];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(35));
        make.size.mas_equalTo(CGSizeMake(120, 30));
    }];
    
    UILabel * idCardTitle = [self lb:@"身份证号:"];
    [idCardTitle  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(horpadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    idCardTf = [self tfWithTag:BaseTag];
    idCardTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [[idCardTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(idCardTitle.mas_right).mas_offset(horInterVer);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(inputW, cellH));
    }];
    
    UILabel * jobNumTitle = [self lb:@"工号:"];
    [jobNumTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(idCardTitle);
        make.top.mas_equalTo(idCardTitle.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    jobNumTf = [self tfWithTag:BaseTag+1];
    [[jobNumTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(jobNumTitle.mas_right).mas_offset(horInterVer);
        make.size.mas_equalTo(CGSizeMake(inputW, cellH));
        make.top.mas_equalTo(idCardTf.mas_bottom).mas_offset(verpadding);
    }];
    
    UILabel * userNameTitle = [self lb:@"手机号:"];
    [userNameTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(idCardTitle);
        make.top.mas_equalTo(jobNumTitle.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    usernameTf = [self tfWithTag:BaseTag+2];
    usernameTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [[usernameTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(userNameTitle.mas_right).mas_offset(horInterVer);
        make.size.mas_equalTo(CGSizeMake(inputW, cellH));
        make.top.mas_equalTo(jobNumTf.mas_bottom).mas_offset(verpadding);
    }];
  
    UILabel * pwdTitle = [self lb:@"密码:"];
    [pwdTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(idCardTitle);
        make.top.mas_equalTo(userNameTitle.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    pwdTf = [self tfWithTag:BaseTag+3];
    pwdTf.secureTextEntry = YES;
    [[pwdTf superview] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdTitle.mas_right).mas_offset(horInterVer);
        make.size.mas_equalTo(CGSizeMake(inputW, cellH));
        make.top.mas_equalTo(usernameTf.mas_bottom).mas_offset(verpadding);
    }];
    
    pwdFlagV = [ UIButton buttonWithType:UIButtonTypeCustom];
    [pwdFlagV addTarget:self action:@selector(pwdSecureAction) forControlEvents:UIControlEventTouchUpInside];
    [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
    [[pwdTf superview] addSubview:pwdFlagV];
    CGFloat h  = VerPxFit(20);
    CGFloat w = h*160/120;
    [pwdFlagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo([pwdTf superview]);
        make.centerY.mas_equalTo(pwdTf.mas_centerY).mas_offset(VerPxFit(0));
        make.size.mas_equalTo(CGSizeMake(w, h));
    }];
    
    UIButton * payBt = [UIButton buttonWithType:UIButtonTypeCustom];
    payBt.centerX = self.centerX;
    [payBt setTitle:@"确 定" forState:UIControlStateNormal];
    [payBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [payBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:payBt];
    [payBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
//        make.size.mas_equalTo(CGSizeMake(HorPxFit(200), VerPxFit(38)));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(65));
    }];
}

- (void)pwdSecureAction{
    if (pwdTf.secureTextEntry) {
        pwdTf.secureTextEntry = NO;
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeOpen"] forState:UIControlStateNormal];
    }else{
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
        pwdTf.secureTextEntry = YES;
    }
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentRight;
    lb.text = text;
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:17];
    [self addSubview:lb];
    return  lb;
}

- (UITextField *)tfWithTag:(NSInteger)tag{
    UIView * view = [[UIView alloc] init];
    [self addSubview:view];
    
    UIImageView * line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(view);
        make.height.mas_equalTo(1.5);
    }];
    
    UITextField * tf = [[UITextField alloc] init];
    tf.tag  = tag;
    tf.returnKeyType = UIReturnKeyNext;
    tf.font = [UIFont systemFontOfSize:16];
    tf.textColor = [UIColor whiteColor];
    tf.delegate = self;
    tf.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.equalTo(view);
        make.bottom.mas_equalTo(line.mas_top);
    }];
    
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    tf.leftView = leftView;
    return tf;
}

- (UIControl *)bkControl{
    if (!_bkControl) {
        CGRect screenBouns = [UIScreen mainScreen].bounds;
        _bkControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, screenBouns.size.width, screenBouns.size.height)];
        _bkControl.backgroundColor = [UIColor blackColor];
        _bkControl.alpha = 0.5;
    }
    return _bkControl;
}

- (void)didMoveToSuperview{
    usernameTf.text = @"";
    pwdTf.text = @"";
    jobNumTf.text = @"";
    idCardTf.text = @"";
}

- (void)showWithSupView:(UIView *)view{
    self.center = view.center;
    self.alpha = 0;
    self.bkControl.alpha = 0;
    [view addSubview:self.bkControl];
    [view addSubview:self];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
        self.bkControl.alpha = 0.5;
    }];
}

- (void)submitAction{

//    if(!idCardTf.text || idCardTf.text.length == 0 || ![BaseHelper checkUserID:idCardTf.text]){
//        [BaseHelper showProgressHud:@"请添加有效身份证号" showLoading:NO canHide:YES];
//        return;
//    }else
        if (!jobNumTf.text || jobNumTf.text.length == 0){
        [BaseHelper showProgressHud:@"请添加工号" showLoading:NO canHide:YES];
        return;
    }else if (!usernameTf.text || usernameTf.text.length == 0){
        [BaseHelper showProgressHud:@"请添加手机号" showLoading:NO canHide:YES];
        return;
    }else if (!pwdTf.text || pwdTf.text.length == 0){
        [BaseHelper showProgressHud:@"请添加密码" showLoading:NO canHide:YES];
        return;
    }
    
    [self.delegate accountSubmitWithIdCard:idCardTf.text jobNum:jobNumTf.text account:usernameTf.text password:pwdTf.text];
}

- (void)cancelAction{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
        self.bkControl.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            [self.bkControl removeFromSuperview];
        }
    }];
}

- (void)dismiss{
    [self cancelAction];
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag == BaseTag+3) {
        [textField resignFirstResponder];
    }else{
        UITextField * tf = [self viewWithTag:textField.tag+1];
        [tf becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == BaseTag+2) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        NSString * validString = @"0123456789";
        if ([validString rangeOfString:string].location != NSNotFound) {
            return YES;
        }else if ([string isEqualToString:@""]){
            return YES;
        }
    }else if (textField.tag == BaseTag){
        if (textField.text.length == 18 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    return YES;
}

@end

