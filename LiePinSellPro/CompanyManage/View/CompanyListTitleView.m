//
//  CompanyListTitleView.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "CompanyListTitleView.h"

@implementation CompanyListTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UILabel * nameLb = [self lb:@"企业名称"];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(170));
    }];
    
    UILabel * shuihaoLb = [self lb:@"企业税号"];
    [shuihaoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(170));
    }];
    
    UILabel * phoneLb = [self lb:@"企业电话"];
    [phoneLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(shuihaoLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(140));
    }];
    
    UILabel * operationLb = [self lb:@"企业操作"];
//    operationLb.backgroundColor = [UIColor blueColor];
    [operationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(140));
    }];
    
    UILabel * companyStateLb = [self lb:@"企业状态"];
//    companyStateLb.backgroundColor = [UIColor redColor];
    companyStateLb.textAlignment = NSTextAlignmentLeft;
    [companyStateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(operationLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(140));
    }];
}


- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:15];
    lb.text = text;
    [self addSubview:lb];
    return lb;
}
@end
