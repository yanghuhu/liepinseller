//
//  CompanyListCell.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"

@protocol CompanyListCellDelegate
//  公司开关切换
- (void)companySwithShift:(CompanyModel *)model;
// 公司信息编辑
- (void)companyInfoEdit:(CompanyModel *)model;

@end

@interface CompanyListCell : UITableViewCell

@property (nonatomic , weak) id<CompanyListCellDelegate>delegate;
@property (nonatomic , strong) CompanyModel * companyModel;

@end
