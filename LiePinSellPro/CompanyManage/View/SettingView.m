//
//  SettingView.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "SettingView.h"
#import "UIView+YYAdd.h"

@interface SettingView(){
    UIView * moneyView;
    UITextField * moneyTf;
}

@property (nonatomic , strong) UIControl * bkControl;

@end

@implementation SettingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UIImage * img = [UIImage imageNamed:@"popBack"];
    UIImageView * bkImg  = [[UIImageView alloc] initWithImage:[img stretchableImageWithLeftCapWidth:img.size.width/2.0 topCapHeight:img.size.height/2.0]];
    [self addSubview:bkImg];
    [bkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    CGFloat selfW = HorPxFit(500);
//    CGFloat selfW = HorPxFit(img.size.width);
    CGFloat selfH = selfW * img.size.height / img.size.width;
    self.frame = CGRectMake(0, 0, selfW, selfH+HorPxFit(80));
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"企业结算方式";
    titleLb.font = [UIFont systemFontOfSize:19];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(50));
        make.size.mas_equalTo(CGSizeMake(200, 30));
    }];
    
    UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBt.frame = CGRectMake(selfW -HorPxFit(75), VerPxFit(30), 30, 30);
    [cancelBt setImage:[UIImage imageNamed:@"cancel"]  forState:UIControlStateNormal];
    [cancelBt addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBt];
    
   
    CGFloat padding = HorPxFit(120);
    CGFloat itemW = 100;
    UIView * companyPay = [self addPayStyleWithTitle:@"企业结算" actionBtTag:100 imageTag:200];
    [companyPay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(padding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(20));
        make.size.mas_equalTo(CGSizeMake(itemW, HorPxFit(50)));
    }];
    
    UIView * orderPay = [self addPayStyleWithTitle:@"订单结算" actionBtTag:101 imageTag:201];
    [orderPay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(companyPay.mas_right).mas_offset(HorPxFit(70));
        make.top.equalTo(companyPay);
        make.size.mas_equalTo(CGSizeMake(itemW, HorPxFit(50)));
    }];
    
    
    moneyView = [[UIView alloc] init];
    moneyView.hidden = YES;
    [self addSubview:moneyView];
    [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(companyPay);
        make.top.mas_equalTo(companyPay.mas_bottom).mas_offset(VerPxFit(10));
        make.right.equalTo(self).mas_offset(-padding);
        make.height.mas_equalTo(HorPxFit(40));
    }];
    
    UILabel * moneyTitle = [[UILabel alloc] init];
    moneyTitle.text = @"充值金额:";
    moneyTitle.font = [UIFont systemFontOfSize:16];
    moneyTitle.textColor = [UIColor whiteColor];
    [moneyView addSubview:moneyTitle];
    [moneyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(moneyView);
        make.height.equalTo(moneyView);
        make.width.mas_equalTo(80);
    }];
    
    moneyTf = [[UITextField alloc] init];
    moneyTf.textColor = [UIColor whiteColor];
    moneyTf.backgroundColor = [UIColor clearColor];
    moneyTf.font = [UIFont systemFontOfSize:18];
    [moneyView addSubview:moneyTf];
    [moneyTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(moneyTitle.mas_right);
        make.width.equalTo(@(HorPxFit(200)));
        make.top.mas_equalTo(moneyTitle.mas_top);
        make.bottom.mas_equalTo(moneyTitle.mas_bottom);
    }];
    
    
//    输入框样式做调整  再不修改原代码基础上增加了styleLine对象
    UIImageView *styleLine  = [[UIImageView alloc] init];
    styleLine.image  = [UIImage imageNamed:@"AccountBottom_short"];
    [moneyTf addSubview:styleLine];
    [styleLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(moneyTf.mas_left);
        make.right.mas_equalTo(orderPay.mas_right);
        make.height.equalTo(@(2));
        make.top.mas_equalTo(moneyTf.mas_bottom).offset(-10);
    }];
    
    
    UIButton * payBt = [UIButton buttonWithType:UIButtonTypeCustom];
    payBt.centerX = self.centerX;
    [payBt setTitle:@"确 定" forState:UIControlStateNormal];
    [payBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [payBt addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:payBt];
    [payBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(175), VerPxFit(33)));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(60));
    }];
}


- (UIView *)addPayStyleWithTitle:(NSString *)title actionBtTag:(NSInteger)btTag imageTag:(NSInteger)imgTag{
    UIView * view = [[UIView alloc] init];
    [self addSubview:view];
    UIImageView * flag = [[UIImageView alloc] init];
    flag.tag = imgTag;
    flag.image = [UIImage imageNamed:@"payUnSele"];
    [view addSubview:flag];
    [flag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view);
        make.centerY.mas_equalTo(view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20,20));
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = title;
    titleLb.font = [UIFont systemFontOfSize:15];
    titleLb.textColor = [UIColor whiteColor];
    [view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(flag.mas_right).mas_offset(HorPxFit(10));
        make.right.and.top.and.bottom.equalTo(view);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    bt.tag = btTag;
    [bt addTarget:self action:@selector(payTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(view);
    }];
    return view;
}

- (void)payTypeChoose:(UIButton *)bt{
    
    UIImageView * unSelImgView;
    UIImageView * selImgView;
    if (bt.tag == 100) {
        unSelImgView = [self viewWithTag:201];
        selImgView = [self viewWithTag:200];
        
        moneyView.hidden = NO;
        self.payType = @"COMPANY_SETTLE";
    }else{
        unSelImgView = [self viewWithTag:200];
        selImgView = [self viewWithTag:201];
        
        moneyView.hidden = YES;
        
        self.payType = @"ORDER_SETTLE";
    }
    unSelImgView.image = [UIImage imageNamed:@"payUnSele"];
    selImgView.image = [UIImage imageNamed:@"paySele"];
}


- (UIControl *)bkControl{
    if (!_bkControl) {
        CGRect screenBouns = [UIScreen mainScreen].bounds;
        _bkControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, screenBouns.size.width, screenBouns.size.height)];
        _bkControl.backgroundColor = [UIColor blackColor];
        _bkControl.alpha = 0.5;
    }
    return _bkControl;
}

- (void)didMoveToSuperview{
    moneyTf.text = @"";
}

- (void)showWithSupView:(UIView *)view{
    self.center = view.center;
    self.alpha = 0;
    self.bkControl.alpha = 0;
    [view addSubview:self.bkControl];
    [view addSubview:self];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
        self.bkControl.alpha = 0.5;
    }];
}

- (void)submitAction{
    
    if (!self.payType || self.payType.length == 0) {
        [BaseHelper showProgressHud:@"请选择结算方式" showLoading:NO canHide:YES];
        return;
    }
    self.money = [moneyTf.text floatValue];
    if ([self.payType isEqualToString:@"COMPANY_SETTLE"] && self.money == 0) {
        [BaseHelper showProgressHud:@"请输入充值金额" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate settingFinishWithType:self.payType];
}

- (void)cancelAction{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
        self.bkControl.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            [self.bkControl removeFromSuperview];
        }
    }];
}

- (void)dismiss{
    [self cancelAction];
}

@end


