//
//  CompanyListHeader.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CompanyListHeaderDelegate
//  添加企业
//- (void)addNewCompany;
// 企业搜索
- (void)searchWithKey:(NSString *)key;
@end

@interface CompanyListHeader : UIView

@property (nonatomic , weak) id<CompanyListHeaderDelegate>delegate;

@end
