//
//  AccountView.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AccountViewDelegate

/*
 * @param  idCard 身份证号
 *         jobNum  工号
 *         account  管理员电话
 *         password  管理员密码
 */

- (void)accountSubmitWithIdCard:(NSString *)idCard jobNum:(NSString *)jobNum account:(NSString *)account password:(NSString *)password;

@end

@interface AccountView : UIView

@property (nonatomic , assign) id<AccountViewDelegate>delegate;

- (void)showWithSupView:(UIView *)view;
- (void)dismiss;

@end
