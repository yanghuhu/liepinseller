//
//  CompanyListCell.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "CompanyListCell.h"
#import "UIImage+YYAdd.h"

@interface CompanyListCell (){
    UILabel * companyNameLb;  //  企业名称
    UILabel * shuihaoLb;      //  税号
    UILabel * phonelb;        //  联系方式
    UIButton * switchBt;
}
@end

@implementation CompanyListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)createSubViews{
    companyNameLb = [self lb];
    [companyNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(170));
    }];
    
    shuihaoLb = [self lb];
    [shuihaoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(companyNameLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(170));
    }];
    
    phonelb = [self lb];
    [phonelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(shuihaoLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(140));
    }];

    UIButton * fixBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [fixBt setImage:[[UIImage imageNamed:@"companyFix"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [fixBt addTarget:self action:@selector(fixAction) forControlEvents:UIControlEventTouchUpInside];
    [fixBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:fixBt];
    [fixBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phonelb.mas_right).mas_offset(HorPxFit(25));
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(90));
    }];
 
    switchBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [switchBt setImage:[UIImage imageNamed:@"companyStop"] forState:UIControlStateNormal];
    [switchBt addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    [switchBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:switchBt];
    [switchBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(fixBt.mas_right).mas_offset(HorPxFit(5));
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(90));
    }];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listCellSep"]];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:15];
    [self addSubview:lb];
    return lb;
}

- (void)switchAction{
    if (!_companyModel.hasHRManager && [_companyModel.state isEqualToString:@"OFF"]) {
        [BaseHelper showProgressHud:@"请先添加企业管理员" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate companySwithShift:_companyModel];
}

- (void)fixAction{
    [self.delegate companyInfoEdit:_companyModel];
}

- (void)setCompanyModel:(CompanyModel *)companyModel{
    _companyModel = companyModel;
    companyNameLb.text = companyModel.name;
    shuihaoLb.text = companyModel.taxNumber;
    phonelb.text = companyModel.contactNumber;
    if ([companyModel.state isEqualToString:@"ON"]) {
        [switchBt setImage:[UIImage imageNamed:@"companyBegin"] forState:UIControlStateNormal];
    }else{
        [switchBt setImage:[UIImage imageNamed:@"companyStop"] forState:UIControlStateNormal];
    }
}

@end
