//
//  SettingView.h
//  LiePinSellPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"

@protocol SettingViewDelegate
// 企业订单支付方式选择
- (void)settingFinishWithType:(NSString *)type;
@end

@interface SettingView : UIView

@property (nonatomic , assign) id<SettingViewDelegate>delegate;
@property (nonatomic , strong) NSString * payType;  // 支付方式
@property (nonatomic , assign) CGFloat money;       //  支付金额

- (void)showWithSupView:(UIView *)view;
- (void)dismiss;

@end
