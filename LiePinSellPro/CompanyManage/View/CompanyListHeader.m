//
//  CompanyListHeader.m
//  LiePinSellPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "CompanyListHeader.h"

@interface CompanyListHeader()<UITextFieldDelegate>{
    UIButton * searchTitleBt;   // 搜索按钮
    UITextField * searchTf;     //  搜索框
}

@end

@implementation CompanyListHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    searchTf = [[UITextField alloc] init];
    searchTf.layer.borderColor = [UIColor lightGrayColor].CGColor;
    searchTf.layer.borderWidth = 1;
    searchTf.leftViewMode = UITextFieldViewModeAlways;
    searchTf.backgroundColor = InputBackColor;
    searchTf.layer.cornerRadius = 34/2;
    searchTf.returnKeyType = UIReturnKeySearch;
    searchTf.delegate = self;
    [self addSubview:searchTf];
    [searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(20));
        make.top.equalTo(self).mas_offset(VerPxFit(4));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(4));
        make.width.mas_equalTo(HorPxFit(180));
    }];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 34/2, 1)];
    searchTf.leftView = view;
    
    searchTitleBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchTitleBt setTitle:@"搜索" forState:UIControlStateNormal];
    searchTitleBt.selected = NO;
    searchTitleBt.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [searchTitleBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchTitleBt addTarget:self action:@selector(searchTitleBtAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:searchTitleBt];
    [searchTitleBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchTf.mas_right).mas_offset(HorPxFit(14));
        make.centerY.mas_equalTo(searchTf.mas_centerY);
        make.width.mas_equalTo(50);
    }];
    
    UIButton * addBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [addBt addTarget:self action:@selector(addBtAction) forControlEvents:UIControlEventTouchUpInside];
    [addBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [addBt setTitle:@"添加企业" forState:UIControlStateNormal];
    addBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [addBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addBt.backgroundColor = [UIColor redColor];
    [self addSubview:addBt];
    [addBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-HorPxFit(20));
        make.height.mas_equalTo(VerPxFit(34));
        make.centerY.mas_equalTo(searchTitleBt.mas_centerY);
        make.width.mas_equalTo(HorPxFit(100));
    }];
}

- (void)addBtAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:TOVIEWCONTROLLER_ADDCOMPANY object:nil];
}

- (void)searchTitleBtAction{
    if (searchTitleBt.selected) {
        [self.delegate searchWithKey:nil];
        searchTitleBt.selected = NO;
        searchTf.text = @"";
        [searchTitleBt setTitle:@"搜索" forState:UIControlStateNormal];
    }else{
        if (!searchTf.text || searchTf.text.length == 0) {
            if (![searchTitleBt.currentTitle isEqualToString:@"取消"]) {
                [BaseHelper showProgressHud:@"请输入搜索关键字" showLoading:NO canHide:YES];
                return;
            }
        }
        searchTitleBt.selected = YES;
        [searchTitleBt setTitle:@"取消" forState:UIControlStateNormal];
        [searchTf resignFirstResponder];
        [self.delegate searchWithKey:searchTf.text];
    }
}

#pragma mark -- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self searchTitleBtAction];
    searchTitleBt.selected = NO;    
    return YES;
}

@end





