//
//  HomeModule.m
//  LiePinSellPro
//
//  Created by Michael on 2018/2/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "HomeModule.h"
#import "NetworkHandle.h"
#import "CompanyModel.h"
#import "NSArray+YYAdd.h"

@implementation HomeModule


+ (void)submitCompanyInfoWithContactNumber:(NSString *)phone
                                  contacts:(NSString *)person
                               companyName:(NSString *)companyName
                                 taxNumber:(NSString *)shuihao
                           businessLicense:(NSString *)businessLicenseUrl
                                   address:(NSString *)address
                                   success:(RequestSuccessBlock)succBlock
                                   failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"address":address,
                            @"businessLicense":businessLicenseUrl,
                            @"contactNumber":phone,
                            @"contacts":person,
                            @"name":companyName,
                            @"taxNumber":shuihao
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    [NetworkHandle postRequestForApi:@"company/create" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            CompanyModel * model = [CompanyModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)updateCompanyInfoWithId:(NSInteger)id_
                  ContactNumber:(NSString *)phone
                       contacts:(NSString *)person
                    companyName:(NSString *)companyName
                      taxNumber:(NSString *)shuihao
                businessLicense:(NSString *)businessLicenseUrl
                        address:(NSString *)address
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"address":address,
                            @"businessLicense":businessLicenseUrl,
                            @"contactNumber":phone,
                            @"contacts":person,
                            @"name":companyName,
                            @"taxNumber":shuihao,
                            @"id":@(id_)
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"company/update" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            CompanyModel * model = [CompanyModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)swithCompnayWihtId:(NSInteger)companyId
                 withState:(NSString *)state
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"id":@(companyId),@"state":state};
    [NetworkHandle postRequestForApi:@"company/setCompanyState" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}


+ (void)submitManageAccountWithUserName:(NSString *)username
                           idCardString:(NSString *)idCard
                                 jobNum:(NSString *)jobNum
                              companyId:(NSInteger)companyId
                               password:(NSString *)password
                                success:(RequestSuccessBlock)succBlock
                                failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"cellphone":username,
                            @"companyId":@(companyId),
                            @"idCardNumber":idCard,
                            @"jobNumber":jobNum,
                            @"password":password,
                            @"verificationCode":@"a"
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"auth/hrManagerCreate" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitCompanyPayTypeWithId:(NSInteger)companyId
                              type:(NSString *)type
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(companyId),@"settleType":type};
    [NetworkHandle postRequestForApi:@"company/setSettleType" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)compayRechargeOrderCrateWihtId:(NSInteger)companyId
                                 Money:(CGFloat)money
                               success:(RequestSuccessBlock)succBlock
                               failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"rechargeBalance":@(money),@"companyId":@(companyId)};
    [NetworkHandle postRequestForApi:@"balanceRecharge/companyBalanceRecharge" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSString * serialNumber = response[@"serialNumber"];
        return @[@YES,serialNumber];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)compayRechargeOrderSure:(NSString *)orderNum
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"serialNumber":orderNum,@"paymentPlatform":@"ALI_PAY"};
    [NetworkHandle postRequestForApi:@"balanceRecharge/payRechargeOrder" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}

+ (void)getCompanyListWithPage:(NSInteger)page
                     searchKey:(NSString *)searchKey
                       success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"page":@(page)};
    NSMutableDictionary * para = [NSMutableDictionary dictionaryWithDictionary:dic];
    if (searchKey && searchKey.length != 0) {
        [para setObject:searchKey forKey:@"name"];
    }
    [NetworkHandle getRequestForApi:@"company/getCompaniesByCreator" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * array = response_[@"content"];
        if (array) {
            NSArray * companyList = [NSArray modelArrayWithClass:[CompanyModel class] json:[array jsonStringEncoded]];
            return @[@YES,companyList];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

@end
