//
//  QRCodeScanManage.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol QRCodeScanManageDelegate

//  扫描成功回调  qrInfo:  二维码信息
- (void)qrScanSuccess:(NSString *)qrInfo;

@end

@interface QRCodeScanManage : NSObject<AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic , strong) UIView * supView;   // 摄像头layer的父view
@property (nonatomic , weak) id<QRCodeScanManageDelegate>delegate;

- (void)initSet;    // 初始化方法
- (void)userFinish;   //扫描完成调用

@end
