//
//  HomeModule.h
//  LiePinSellPro
//
//  Created by Michael on 2018/2/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeModule : NSObject

/*
 *  创建企业
 *  prarm :
 *      phone: 联系电话
 *      person: 联系人
 *      companyName :  企业名称
 *      shuihao ：税号
 *      address： 办公地点
 *      businessLicense ： 营业执照图片
 */
+ (void)submitCompanyInfoWithContactNumber:(NSString *)phone
                                   contacts:(NSString *)person
                                companyName:(NSString *)companyName
                                  taxNumber:(NSString *)shuihao
                            businessLicense:(NSString *)businessLicenseUrl
                                    address:(NSString *)address
                                    success:(RequestSuccessBlock)succBlock
                                    failure:(RequestFailureBlock)failBlock;

/*
 *  更新企业信息
 *  prarm :
 *      phone: 联系电话
 *      person: 联系人
 *      companyName :  企业名称
 *      shuihao ：税号
 *      address： 办公地点
 *      businessLicense ： 营业执照图片
 */
+ (void)updateCompanyInfoWithId:(NSInteger)id_
                  ContactNumber:(NSString *)phone
                       contacts:(NSString *)person
                    companyName:(NSString *)companyName
                      taxNumber:(NSString *)shuihao
                businessLicense:(NSString *)businessLicenseUrl
                        address:(NSString *)address
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/*
 *  切换公司开关状态
 *  @param
 *    companyId  公司id
 *    state    ['ON', 'OFF']
 */
+ (void)swithCompnayWihtId:(NSInteger)companyId
                 withState:(NSString *)state
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;

/*
 *  @param
 *      username  登录账号
 *      idCard   身份照号
 *      jobNum  工号
 *      password  登录密码
 *  return
 *       nil
 */
+ (void)submitManageAccountWithUserName:(NSString *)username
                           idCardString:(NSString *)idCard
                                 jobNum:(NSString *)jobNum
                              companyId:(NSInteger)companyId
                               password:(NSString *)password
                                success:(RequestSuccessBlock)succBlock
                                failure:(RequestFailureBlock)failBlock;

/*
 *  @param
 *      companyId   企业id
 *      type    支付方式   COMPANY_SETTLE', 'ORDER_SETTLE
 */
+ (void)submitCompanyPayTypeWithId:(NSInteger)companyId
                               type:(NSString *)type
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock;


/*
 *  @param
 *    money  金额
 */

+ (void)compayRechargeOrderCrateWihtId:(NSInteger)companyId
                                  Money:(CGFloat)money
                                success:(RequestSuccessBlock)succBlock
                                failure:(RequestFailureBlock)failBlock;

/*
 *  @param
 *      orderNum  订单号
 */
+ (void)compayRechargeOrderSure:(NSString *)orderNum
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/*
 *  获取企业列表
 *  @param
 *      num    页码
 *   return
 *      [CompanyModel]
 */
+ (void)getCompanyListWithPage:(NSInteger)num
                     searchKey:(NSString *)searchKey
                       success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock;




@end

