//
//  BaseMockData.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "BaseMockData.h"
#import "NSString+YYAdd.h"

@implementation BaseMockData

+ (instancetype)createWithFunctionName:(NSString *)functionName
{
    @throw [NSException exceptionWithName:@"SubClass_Must_Be_Implement" reason:@"子类必须要实现此方法" userInfo:nil];
}

+ (instancetype)createWithFunctionName:(NSString *)functionName forceMockData:(BOOL)forceMockData
{
    @throw [NSException exceptionWithName:@"SubClass_Must_Be_Implement" reason:@"子类必须要实现此方法" userInfo:nil];
}

//全局设置模拟数据
+ (BOOL)isNeedMockData
{
#if DEBUG
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].needMock) {
        return YES;
    }else{
        return NO;
    }
#else//release模式下强制不走模拟数据
    return NO;
#endif
    
}

- (NSMutableDictionary *)callbackDicData
{
    if (_callbackDicData==nil) {
        _callbackDicData = [NSMutableDictionary dictionary];
    }
    return _callbackDicData;
}

- (NSMutableArray *)callbackData
{
    if (_callbackData==nil) {
        _callbackData = [[NSMutableArray alloc] init];
    }
    return _callbackData;
}

- (void)mockData
{
    //    @throw [NSException exceptionWithName:@"SubClass_Must_Be_Implement" reason:@"子类必须要实现此方法" userInfo:nil];
    SEL selector = NSSelectorFromString(self.functionName);
    if (selector && [self respondsToSelector:selector]) {
        //            [self performSelector:selector];//有警告//http://blog.csdn.net/zsk_zane/article/details/47426805
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self,selector);
    }
}

- (BaseMockData *)Excute
{
    if (self.functionName && self.functionName.length != 0) {
        self.functionName = [self.functionName stringByReplacingOccurrencesOfString:@":" withString:@"_"];
        [self mockData];
    }
    return self;
}

@end
