//
//  BaseViewController.h
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+YYModel.h"
#import "MJRefresh.h"

typedef enum {
    TableViewDataSourceChangeForRefresh = 0,
    TableViewDataSourceChangeForGetMore,
}TableViewDataSourceChange;

@interface BaseViewController : UIViewController

- (void)createSubViews;
- (void)loadData;
- (void)netFailWihtError:(NSString *)error andStatusCode:(NSInteger)statusCode;

@end
