//
//  FlyTextFile.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FlyTextFile;

@protocol FlyTextFileDelegate

@optional
- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)textFieldShouldReturn:(FlyTextFile*)textField;

@end


@interface FlyTextFile : UIView

@property (nonatomic , strong) UIFont * Titlefont;
@property (nonatomic , strong) UIFont * font;
@property (nonatomic , strong) NSString * text;
@property (nonatomic , weak) id<FlyTextFileDelegate>delegate;
@property (nullable, nonatomic,copy)   NSString *placeholder;          // default is nil. string is drawn 70% gray
@property(nonatomic) UIReturnKeyType returnKeyType;
@property(nonatomic) UIKeyboardType keyboardType;   
@property(nonatomic,getter=isSecureTextEntry) BOOL secureTextEntry;    

@property (nonatomic , assign) CGFloat titleLbH;

@property (nonatomic , assign) BOOL needDropDownList;

- (void)createSubViews;

@end
