//
//  TTTextField.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TTTextField.h"

#define TextPlaceHolderColor [UIColor lightGrayColor]

@interface TTTextField()<UITextFieldDelegate>{
    UILabel * titleLb;
    UITextField * tf;
}
@end

@implementation TTTextField
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
        
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    titleLb = [[UILabel alloc] init];
    titleLb.font = [UIFont systemFontOfSize:12];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.backgroundColor = [UIColor clearColor];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.equalTo(self);
        make.height.mas_equalTo(17);
    }];
    
    UIImageView * bottomline = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [self addSubview:bottomline];
    [bottomline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(titleLb);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    tf = [[UITextField alloc] init];
    tf.font = [UIFont systemFontOfSize:14];
    tf.delegate = self;
    tf.borderStyle = UITextBorderStyleNone;
    tf.textColor = TextPlaceHolderColor;
    [self addSubview:tf];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 1)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLb.mas_bottom);
        make.bottom.mas_equalTo(bottomline.mas_top);
        make.left.and.right.equalTo(self);
    }];
    _isTfHadText = NO;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    titleLb.text = title;
}

- (void)setText:(NSString *)text{
    tf.text = text;
}

- (NSString *)text{
    return tf.text;
}
- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
//    tf.placeholder = placeholder;
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType{
    tf.keyboardType = keyboardType;
}

#pragma mark -- UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (!self.delegate) {
        return;
    }
    [self.delegate textFieldDidBeginEditing:self];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text && textField.text.length !=0) {
        textField.textColor = [UIColor whiteColor];
        _isTfHadText = YES;
    }else{
        textField.textColor = TextPlaceHolderColor;
        textField.text = _placeholder;
    }
    [self.delegate textFieldDidEndEditing:self];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (!_isTfHadText) {
        textField.text = nil;
        textField.textColor = [UIColor whiteColor];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (!self.delegate) {
        return YES;
    }
    return  [self.delegate textField:self shouldChangeCharactersInRange:range replacementString:string];
    
}

@end
