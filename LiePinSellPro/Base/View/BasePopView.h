//
//  BasePopView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasePopView : UIView{
    UIControl * bkControl;    
}

- (UIView *)supView:(UIView *)view;
- (void)showWithSupView:(UIView *)view;
- (void)dismissAction;
- (void)addTopRightDismissBt;

@end
