//
//  BaseModel.h
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+YYModel.h"

@interface BaseModel : NSObject

@end
