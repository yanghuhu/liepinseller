//
//  BaseModule.m
//  JLGProject
//
//  Created by yang on 2017/3/1.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "BaseModule.h"

@implementation BaseModule

+ (NSDictionary *)reqestParam:(NSDictionary *)info{

    if (!info) {
        info = [NSDictionary dictionary];
    }
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithDictionary:info];
    return dictionary;
}

@end
